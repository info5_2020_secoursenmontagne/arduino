

/*Variables d'initialisations LoRa*/
int8_t trPower = 1;         // Transreceiver power  ( can be -3 to 15)
String SprFactor = "sf12";  // Spreadingsfactor     (can be sf7 to sf12)
uint8_t max_dataSize = 100; // Maximum charcount to avoid writing outside of string
unsigned long readDelay = 3000; // Time to read for messages in ms (max 4294967295 ms, 0 to disable)
const char CR = '\r';
const char LF = '\n';
const unsigned long FREQ = 868500000;


/*Variables de stockage du message*/
char test[45];
char mdp[3] = {'/','*','-'};
int i = 0;

/* Fonction d'initialisation du programme*/
void setup() {

  /*Nécessaire LoRa*/
  Serial2.begin(57600); // Serial vers puce LoRa
  LoraP2P_Setup();      // Fonctions d'initialisation de la puce LoRa
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(LED_GREEN, OUTPUT);
  pinMode(LED_BLUE, OUTPUT);

  /*Nécessaire Bluetooth*/
  Serial.begin(38400);   // Serial vers le module bluetooth

  /*Nécessaire debug USB*/
  SerialUSB.begin(38400);// Optionnel, serial vers l'USB.

  /*Gestion des LEDs*/
  digitalWrite(LED_GREEN, HIGH);
  digitalWrite(LED_BLUE, HIGH);

}

/* Boucle principale du programme*/
void loop() {

  SerialUSB.println("loop");

  /* Fonctionnement normal :
     - Si reception LoRA -> Bluetooth
     - Si reception Bluetooth -> LoRa
  */

  char Data[100] = "";

  /*En cas de reception d'un message LoRa*/
  if (LORA_Read(Data) == 1) {

    digitalWrite(LED_BLUE, LOW);
    /*On le convertis en string*/
    hexStrToStr(Data);

    /*On récupère le SNR*/
    FlushSerialBufferIn();
    Serial2.write("radio get snr\r\n");
    delay(100);

    /*On rajoute le SNR à la fin du message*/
    int decalage = 37;
    
    int nb = Serial2.available();
    for(int i=0;i<nb; i++){
      if(i<(nb-2)) {
        Data[decalage] = Serial2.read();
        decalage++;
      }else {
        Serial2.read();
      }
    }
    
    nb=nb-2;
    for(int j=nb; j<3; j++) {
      Data[decalage] = '/';
      decalage++;
    }

    SerialUSB.println("Message LoRa received, sending to bluetooth");
    /*On envoie le message en Bluetooth*/
    Serial.println(Data);
    digitalWrite(LED_BLUE, HIGH);
  }

  /*En cas de reception de messages bluetooth*/
  while(Serial.available()){
    
    digitalWrite(LED_GREEN, LOW);

    /*On vérifie le mot de passe du message*/
    test[i] = Serial.read();
    if(i<3) {
      if(test[i]==mdp[i]) {
        i++;
      }else {
        i=0;
      }
    }else {
      i++;
    }

    /*Quand le message est complet*/
    if(i==40) {
      i=0;

      /*On le convertis en hexa*/
      int len = strlen(test);
      char hex_str[(len*2)+1];
      string2hexString(test, hex_str);


      SerialUSB.println("Message Bluetooth received, sending to LoRa");
      /*Et on l'envoi en LoRa, 2 fois car lors du changement de modes (reception->emission) le premier envoi est raté*/
      LORA_Write(hex_str);
      LORA_Write(hex_str);
      
    }
    digitalWrite(LED_GREEN, HIGH);
  }
  
}



/* Fonctions utilitaires, ou d'initialisation */


//function to convert ascii char[] to hex-string (char[])
void string2hexString(char* input, char* output)
{
    int loop;
    int i; 
    
    i=0;
    loop=0;
    
    while(input[loop] != '\0')
    {
        sprintf((char*)(output+i),"%02X", input[loop]);
        loop+=1;
        i+=2;
    }
    //insert NULL at the end of the output string
    output[i++] = '\0';
}

void hexStrToStr(char *x_hex_string){
  
  char sub_payload[2+1];
  uint8_t x_index=0;

  for(uint8_t i=0; i<(uint8_t)strlen(x_hex_string); i=i+2){
    strncpy(sub_payload, x_hex_string+i, 2);
    sub_payload[2] = '\0';
    x_hex_string[x_index] = (char)strtol(&sub_payload[0], NULL, 16);
    x_index++;
  }
  x_hex_string[x_index] = '\0';
}

// Configuring the RN2483 for P2P
void LoraP2P_Setup()
{

  Serial2.print("sys reset\r\n");
  delay(200);
  Serial2.print("radio set pwr ");
  Serial2.print(trPower);
  Serial2.print("\r\n");
  delay(100);
  Serial2.print("radio set sf ");
  Serial2.print(SprFactor);
  Serial2.print("\r\n");
  delay(100);
  Serial2.print("radio set wdt ");
  Serial2.print(readDelay);
  Serial2.print("\r\n");
  delay(100);
  Serial2.print("mac pause\r\n");
  delay(100);
  Serial2.print("radio set freq ");
  Serial2.print(FREQ);
  Serial2.print("\r\n");
  delay(100);


  FlushSerialBufferIn();
}

// Flushes any message available (depuis la puce LoRa)
void FlushSerialBufferIn()
{
  while (Serial2.available() > 0)
  {
    #ifdef DEBUG
        SerialUSB.write(Serial2.read());
    #else
        Serial2.read();
    #endif
  }
}

// Setting up the receiver to read for incomming messages
void StartLoraRead()
{
  Serial2.print("radio rx 0\r\n");
  delay(100);

  FlushSerialBufferIn();
}

/* Fonctions d'envoi d'un message en LoRa */
// Send Data array (in HEX)
void LORA_Write(char* Data)
{
  
  Serial2.print("radio tx ");
  Serial2.print(Data);
  Serial2.print("\r\n");
  Serial2.flush();
  
  waitTillMessageGone();  
  
}

// Waits until the data transmit is done
void waitTillMessageGone()
{
  while (!Serial2.available());
  delay(10);
  while (Serial2.available() > 0) {
    //SerialUSB.write(Serial2.read());
    Serial2.read();
  }
    
  while (!Serial2.available());
  delay(10);
  while (Serial2.available() > 0)
  {
    //SerialUSB.write(Serial2.read());
    Serial2.read();
  }
}

/* Fonctions de reception d'un message LoRa */
//////////////////////////////////////
// Read message from P2P TX module  //
// Returns 1 if there is a message  //
// Returns 2 if there is no message //
//////////////////////////////////////
int LORA_Read(char* Data)
{
  int messageFlag = 0;
  String dataStr = "radio_rx  ";
  String errorStr = "radio_err";
  String Buffer = "";

  StartLoraRead();

  while (messageFlag == 0) // As long as there is no message
  {
    /*if (!Serial2.available()) {
      messageFlag = 2;
    }else {
    */

      while (!Serial2.available());
      delay(50);  // Some time for the buffer to fill

      // Read message from RN2483 LORA chip
      while (Serial2.available() > 0 && Serial2.peek() != LF)
      {
        Buffer += (char)Serial2.read();
      }
  
      // If there is an incoming message
      if (Buffer.startsWith(dataStr, 0)) // if there is a message in the buffer
      {
        int i = 10;  // Incoming data starts at the 11th character
  
        // Seperate message from string till end of datastring
        while (Buffer[i] != CR && i - 10 < max_dataSize)
        {
          Data[i - 10] = Buffer[i];
          i++;
        }
        messageFlag = 1; // Message received
      }
      else if (Buffer.startsWith(errorStr, 0))
      {
        messageFlag = 2; // Read error or Watchdogtimer timeout
      }
    }
  

  #ifdef DEBUG
    SerialUSB.println(Buffer);
  #endif

  return (messageFlag);
}
